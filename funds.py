from rest_framework import status, views
from rest_framework.permissions import AllowAny
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from authentication.permissions.authenticated_only_read import AuthenticatedOnlyRead
from authentication.permissions.is_mode1 import IsMode1
from funds.models import FundsAvailable
from funds.serializers.funds_serializer import FundsSerializer
from funds.serializers.funds_write_serializer import FundsWriteSerializer


class FundsView(views.APIView):
    permission_classes = [IsAuthenticated, IsMode1]
    serializer_class = FundsSerializer

    def get(self, request, uid=None):
        if uid:
            instance = FundsAvailable.objects.get(pk=uid, user=request.user)
            serializer = self.serializer_class(instance)
            return Response(serializer.data, status=status.HTTP_200_OK)

        instance = FundsAvailable.objects.all().order_by('created_ts')
        serializer = self.serializer_class(instance, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        user = request.user
        data = request.data
        try:
            output = self._save(user, data)
        except ValueError as e:
            return Response(str(e), status=status.HTTP_406_NOT_ACCEPTABLE)

        return Response(output, status=status.HTTP_201_CREATED)

    def put(self, request, uid):
        user = request.user
        data = request.data
        try:
            output = self._update(user, data, uid)
        except ValueError as e:
            return Response(str(e), status=status.HTTP_406_NOT_ACCEPTABLE)

        return Response(output, status=status.HTTP_200_OK)

    def delete(self, request, uid):
        user = request.user
        fund = user.available_funds.get(pk=uid)
        trades = user.trades.filter(trade_at_broker=fund.trade_at_broker)
        fund.delete()
        trades.delete()

        return Response({}, status=status.HTTP_204_NO_CONTENT)

    def _save(self, user, data):
        if self._is_same_broker_exists(data['trade_at_broker'], user):
            raise ValueError('This broker already selected in the other fund')

        serializer = FundsWriteSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save(user=user)
        return serializer.data

    def _is_same_broker_exists(self, broker_id, user):
        return user.available_funds.filter(trade_at_broker_id=broker_id).exists()

    def _update(self, user, data, uid):
        if self._is_same_broker_exists(data['trade_at_broker'], user):
            raise ValueError('This broker already selected in the other fund')

        fund = user.available_funds.get(pk=uid)
        serializer = FundsWriteSerializer(fund, data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return serializer.data


