from decimal import Decimal
from itertools import islice
from common.models import Stocks
from stocks.models import StockPriceLive


class StockPriceLiveComponent(object):
    def __init__(self):
        pass

    def save_bulk(self, results):
        count_results = len(results)
        batch_size = 100
        count_saved = 0

        objs = self._get_models(results)
        while True:
            batch = list(islice(objs, batch_size))
            if not batch:
                break
            else:
                created_prices = StockPriceLive.objects.bulk_create(batch, batch_size)
                count_saved += len(created_prices)

        return count_results, count_saved

    def serialize(self, data):
        sym = data.get('sym')
        tick_volume = data.get('v', 0)
        accumulated_volume = data.get('av', 0)
        open = data.get('op', 0)
        tick_volume_weighted = data.get('vw', 0)
        open_tick_price = data.get('o', 0)
        closing_tick_price = data.get('c', 0)
        highest_tick_price = data.get('h', 0)
        lowest_tick_price = data.get('l', 0)
        volume_weighted_price = data.get('a', 0)
        average_trade_size = data.get('z', 0)
        ts_starting_tick = data.get('s', 0)
        ts_ending_tick = data.get('e', 0)

        data = {
            "ticker_symbol": sym,
            "tick_volume": int(tick_volume),
            "accumulated_volume": int(accumulated_volume),
            "open": Decimal(open),
            "tick_volume_weighted": Decimal(tick_volume_weighted),
            "open_tick_price": Decimal(open_tick_price),
            "closing_tick_price": Decimal(closing_tick_price),
            "highest_tick_price": Decimal(highest_tick_price),
            "lowest_tick_price": Decimal(lowest_tick_price),
            "volume_weighted_price": Decimal(volume_weighted_price),
            "average_trade_size": int(average_trade_size),
            "ts_starting_tick": int(ts_starting_tick),
            "ts_ending_tick": int(ts_ending_tick),
        }
        return data

    def _get_models(self, results):
        for count, live_price in enumerate(results):
            try:
                symbol=live_price['sym']
            except:
                continue
            try:
                stock = Stocks.objects.get(symbol=symbol)
            except Stocks.DoesNotExist:
                continue
            item = self.serialize(live_price)
            item['stock_id'] = stock.id

            yield StockPriceLive(**item)

    # {
    #     "ev": "AM",
    #     "sym": "GTE",
    #     "v": 4110,
    #     "av": 9470157,
    #     "op": 0.4372,
    #     "vw": 0.4488,
    #     "o": 0.4488,
    #     "c": 0.4486,
    #     "h": 0.4489,
    #     "l": 0.4486,
    #     "a": 0.4352,
    #     "z": 685,
    #     "s": 1610144640000,
    #     "e": 1610144700000
    # }
