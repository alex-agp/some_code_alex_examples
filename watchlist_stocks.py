from rest_framework import status, views
from rest_framework.permissions import AllowAny
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from common.models import Stocks
from common.serializers.stocks_and_last_price_serializer import StocksAndLastPriceSerializer
from common.serializers.paginations.basic_pagination import BasicPagination
from common.serializers.paginations.pagination_handler_mixin import PaginationHandlerMixin


class WatchlistStocksView(views.APIView, PaginationHandlerMixin):
    permission_classes = [IsAuthenticated]
    pagination_class = BasicPagination
    serializer_class = StocksAndLastPriceSerializer

    def get(self, request, uid):
        watch_list = request.user.watch_lists.get(pk=uid)
        stocks = watch_list.stocks.order_by('symbol')
        page = self.paginate_queryset(stocks)

        if page is not None:
            serializer = self.get_paginated_response(self.serializer_class(page,many=True).data)
        else:
            serializer = self.serializer_class(stocks, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, uid):
        watch_list = request.user.watch_lists.get(pk=uid)
        stocks = request.data

        for item in stocks:
            stock = Stocks.objects.get(pk=item['id'])

            if item['isChecked'] is True:
                watch_list.stocks.add(stock)
            elif item['isChecked'] is False:
                watch_list.stocks.remove(stock)

        return Response(stocks, status=status.HTTP_200_OK)

    def delete(self, request, uid, sid):
        user = request.user
        watch_list = user.watch_lists.get(pk=uid)
        stock = watch_list.stocks.get(pk=sid)
        watch_list.stocks.remove(stock)

        return Response({}, status=status.HTTP_204_NO_CONTENT)