import os, time, json
import logging
import asyncio
import signal
import threading
import websockets
import redis
from django.conf import settings
from django.core import management
from django.core.management.base import BaseCommand
from polygon import STOCKS_CLUSTER#, WebSocketClient
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from stocks.tasks import save_stock_price_live
from stocks.components.polygon.websocket_client import WebSocketClient


class Command(BaseCommand):
    help = 'Updates stocks_prices'

    def handle(self, *args, **options):
        # self.run_web_socket()
        self.r = redis.Redis(host='redis', port=6379, decode_responses=True)
        self.enable_saving = False
        self._thread = None
        signal.signal(signal.SIGINT, self._cleanup_signal_handler())
        signal.signal(signal.SIGTERM, self._cleanup_signal_handler())
        self.keep_live = True
        self.keep_live_seconds = False  # 60 * 2 # use False to run constantly
        self.stdout.write('===Websocket connection will exist {} seconds'.format(self.keep_live_seconds))
        self.run_thread()  #  todo restart thread if connection will be closed into self._close_handler, self._error_handler
        self.stdout.write('Done')

    def run_thread(self):
        self._thread = threading.Thread(target=self.run_listener)
        self._thread.start()
        self._thread.join()

    def run_listener(self):
        client = self.run_web_socket()
        i = 0
        while (self.keep_live_seconds is False or i < self.keep_live_seconds) and self.keep_live is True:
            try:
                time.sleep(1)
                i += 1

            except (KeyboardInterrupt, SystemExit):
                print('-----WSKeyboardInterrupt----')
                raise

            except Exception as e:
                print('-----WSException----{}'.format(str(e)))

        client.close_connection()

    def run_web_socket(self):
        client = WebSocketClient(STOCKS_CLUSTER, settings.POLYGON_API_KEY, self.event_websocket_message, self._close_handler, self._error_handler)
        client.run_async()

        client.subscribe("A.*")  # "A.MSFT", "T.AAC" # todo should be AM.* ?
        # time.sleep(25)
        # client.close_connection()
        return client
    
    def event_websocket_message(self, message):
        self._send_message_to_stock_websocket(message)
        # todo put more messages to list at once, todo create a separate db to prevent high loading in the main db
        if self.enable_saving:
            task = save_stock_price_live.apply_async(args=[message], queue='numerous')

    def close_connection(self):
        self.keep_live = False
        self.stdout.write('---closed_connection---')
        
    def _error_handler(self, ws, error):
        self.keep_live = False
        self.stdout.write("---error handler---", error)

    def _close_handler(self, ws, error, error2):
        self.keep_live = False
        self.stdout.write("---close handler---")
        
    def _cleanup_signal_handler(self):
        return lambda signalnum, frame: self.close_connection()

    def _send_message_to_stock_websocket(self, messages):
        data = json.loads(messages)
        for mess in data:
            sym = mess.get('sym')
            if sym:
                self.r.publish(sym, json.dumps([mess]))

        # channel_layer = get_channel_layer()
        # async_to_sync(channel_layer.group_send)(
        #     'poligon_api_input_data',
        #     {
        #         'type': 'refresh_from_poligon_api',
        #         'data': json.loads(message),
        #     }
        # )

        # self.stdout.write(messages)

