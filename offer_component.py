from buyer.models import Offer
from supplier.components.bid import Bid as BidComponent
from supplier.models import Bid as BidModel
from supplier.models import VolumeBasedFee


class OfferComponent(object):
    def __init__(self):
        pass

    def get_currencies(self, offer):
        currencies = []
        volume_based_fees = offer.bid.volume_based_fees.all()
        per_usage_fees = offer.bid.per_usage_fees.all()

        for volume_based_fee in volume_based_fees:
            currencies.append(volume_based_fee.price.price.currency)
        for per_usage_fee in per_usage_fees:
            currencies.append(per_usage_fee.price.currency)

        currencies.extend([
            offer.bid.differential.price.currency,
            offer.bid.dftc.price.currency
        ])

        if offer.bid.market_price:
            currencies.append(offer.bid.market_price.price.price.currency)

        return currencies

    def copy(self, company, offer, offer_status=Offer.Statuses.AWAITING, bid_status=BidModel.Statuses.ACTIVE):
        return Offer(**{
            'name': offer.name,
            'bid': BidComponent(company).copy_bid(offer.bid, {'status': bid_status}),
            'supplier': offer.supplier,
            'status': offer_status
        })

    @staticmethod
    def apply_related_fields(offers_queryset):
        offers_queryset = offers_queryset.select_related(
            'bid', 'supplier', 'bid__supply_condition', 'bid__total_price',
            'bid__total_price__buyer_price',
            'bid__total_price__buyer_price__currency',
            'bid__total_price__supplier_price',
            'bid__total_price__supplier_price__currency',
            'bid__base_price', 'bid__base_price__buyer_price', 'bid__base_price__supplier_price',
            'bid__base_price__supplier_price__currency', 'bid__base_price__buyer_price__currency',
            'bid__differential', 'bid__differential__units', 'bid__differential__price',
            'bid__differential__price__currency',
            'bid__market_price', 'bid__market_price__price', 'bid__market_price__price__units',
            'bid__market_price__price__price',
            'bid__market_price__price__price__currency',
            'bid__ramp_limitations', 'bid__fuel_availability',
            'bid__supply_condition',
            'bid__payment'
        )
        offers_queryset = offers_queryset.prefetch_related(
            'bid__supply_condition__payment_methods',
            'bid__volume_based_fees',
            'bid__per_usage_fees',
            'bid__non_mandatory_fees',
            'bid__non_mandatory_volume_based_fees',
            'bid__supply_condition__payment_methods',
        )
        return offers_queryset

    @staticmethod
    def apply_full_related_fields(offers):
        offers = offers.select_related('round', 'bid', 'bid__fuel_preference', 'supplier', 'round__contract',
                                       'round__contract__location', 'bid__fuel_type',
                                       'bid__market_price__price', 'bid__index', 'bid__index__index_code',
                                       'bid__index__averaging_method', 'bid__index__averaging_offset',
                                       'bid__market_price', 'bid__index__bate', 'bid__index__effective_day',
                                       'bid__market_price__price__price__currency',
                                       'bid__market_price__price__units', 'bid__total_price',
                                       'bid__total_price__buyer_price', 'bid__market_price__source_type',
                                       'bid__total_price__buyer_price__currency', 'bid__differential',
                                       'bid__differential__price',
                                       'bid__differential__price__currency', 'bid__differential__units',
                                       'bid__base_price',
                                       'bid__base_price__buyer_price', 'bid__base_price__buyer_price__currency',
                                       'bid__payment',
                                       'bid__payment__open_invoice',
                                       'bid__payment__unit', 'bid__payment__currency',
                                       'bid__payment__prepayment', 'bid__payment__prepayment__payment_frequency',
                                       'bid__payment__method_of_payment', 'bid__payment__exchange_rate',
                                       'bid__payment__exchange_rate__financial_source',
                                       'bid__payment__exchange_rate__averageing_method',
                                       'bid__payment__exchange_rate__averaging_offset',
                                       'bid__payment__open_invoice__payment_reference_date',
                                       'bid__payment__open_invoice__invoice_frequency',
                                       'bid__payment__open_invoice__invoice_type',
                                       'bid__ramp_limitations',
                                       'bid__fuel_availability', 'bid__gross_net_billing') \
            .prefetch_related('bid__per_usage_fees', 'bid__per_usage_fees__price',
                              'bid__per_usage_fees__price__currency',
                              'bid__volume_based_fees', 'bid__volume_based_fees__price',
                              'bid__volume_based_fees__price__units',
                              'bid__volume_based_fees__price__price', 'bid__volume_based_fees__price__price__currency',
                              'bid__non_mandatory_fees', 'bid__non_mandatory_fees__price',
                              'bid__non_mandatory_fees__currency')
        return offers

