import csv
from itertools import islice
from rest_framework import status, views
from rest_framework.permissions import AllowAny
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from typing import Iterator, Tuple
from authentication.permissions.is_superuser import IsSuperuser
from common.models import Stocks, BusinessSector, BusinessIndustry


class ImportStocksView(views.APIView):
    permission_classes = [IsAuthenticated, IsSuperuser]

    def post(self, request):
        user = request.user
        file = request.FILES.get('file')
        if not file:
            return Response({'details': 'File not found'}, status=status.HTTP_406_NOT_ACCEPTABLE)

        count = self._import_stocks(file)

        return Response({'details': {'count': count}}, status=status.HTTP_200_OK)

    def _import_stocks(self, file, delete_all=True):
        if delete_all is True:
            Stocks.objects.all().delete()

        count = 0
        batch_size = 1000
        stocks = file.read().decode('utf-8').splitlines()
        reader = csv.DictReader(stocks)
        stocks = self._get_stocks_bunch(reader)

        while True:
            batch = list(islice(stocks, batch_size))
            if not batch:
                break
            created_stocks = Stocks.objects.bulk_create(batch, batch_size, ignore_conflicts=True)
            count += len(created_stocks)

        return count

    def _get_stocks_bunch(self, reader) -> Iterator:
        for row in reader:
            business_sector_id = None
            business_industry_id = None

            if _null_to_blank(row['sector']):
                business_sector_id = BusinessSector.objects.get_or_create(name=row['sector'])[0].id
            if _null_to_blank(row['industry']):
                business_industry_id = BusinessIndustry.objects.get_or_create(name=row['industry'])[0].id

            symbol = row['symbol']
            name = row['name']
            country = _null_to_blank(row['country'])
            ipo_year = _null_to_blank(row['ipo_year'])

            stock = Stocks(
                symbol=symbol,
                name=name,
                country=country,
                ipo_year=ipo_year,
                business_sector_id=business_sector_id,
                business_industry_id=business_industry_id)
            yield stock


def _null_to_blank(name):
    if name == 'null':
        return None
    return name
